sniffa
======

sniffa is a small utility that allows you to watch Discuss forums for keywords.

Originally this program was developed [by Daniel Mitterdorfer](https://github.com/danielmitterdorfer/sniffa). This fork removes the MacOS dependency and make it work with GNU/Linux and GNOME3. Other desktops should work as well, but I didn't tested it.

Every time it is invoked, it checks for new posts matching the keywords and creates a notification.

# Requirements

* GNU/Linux with python-gobject and libnotify-bin installed
* Python 3
* certifi: Install with `pip3 install certifi`

# Installation

Ensure that all prerequisites are installed, then copy `sniffa.py` to any directory, e.g. `~/bin` and run `chmod u+x sniffa.py`.

# Usage

sniffa can be used to query multiple Discuss forums. The keywords and the ids of all already seen posts are maintained in a file `~/.sniffa/watch-$(FORUM_NAME).ini`, where `$(FORUM_NAME)` is a name that you can choose to identify this forum.

## Example

Consider you want to watch for the keywords "Talk" and "Calendar" in the Nextcloud forum to get notified if someone writes something about your favourite Nextcloud apps.

1. Create `~/.sniffa/watches-nextcloud.ini`
2. Add the following lines:

```
[sniffa.domain]
url = https://help.nextcloud.com

[Talk]

[Calendar]
```

Now invoke sniffa: ``python3 sniffa.py elastic``. It will load the watches file for the forum named "nextcloud", check for new posts (which will be a lot at the first time) and show a notice for each of them in the Gnome notification bar.

## Automatic regular invocation

to call sniffa regularly in the background you can write a small bash script `sniffa.sh`:

```
#!/bin/sh  
while true  
do  
    python3 ~/bin/sniffa.py nextcloud &> /dev/null
    sleep 300  
done
```

copy it to `~/bin` and run `chmod 755 sniffa.sh`.

Afterwards add it to the `autostart` by creating `sniffa.desktop` in `~/.config/autostart`:

```
[Desktop Entry]
Name=Sniffa
Comment=Watch Discuss forums for keywords 
Exec=/home/schiesbn/bin/sniffa.sh
Terminal=false
Type=Application
```

# License

'sniffa' is distributed under the terms of the [Apache Software Foundation license, version 2.0](http://www.apache.org/licenses/LICENSE-2.0.html).

